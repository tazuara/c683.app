# C683 Paper Airplanes Plan View Surface Measurer

A tool to measure the surface area of high contrast shapes.

## References

  * https://stackoverflow.com/a/63163667
  * https://stackoverflow.com/a/57597494/496351
  * https://swiftwithmajid.com/2020/11/04/how-to-use-geometryreader-without-breaking-swiftui-layout/
  * https://img.ly/blog/how-to-create-image-filters-in-ios/
  * https://cifilter.io/CIAffineTransform/
  * https://betterprogramming.pub/new-in-ios-14-vision-contour-detection-68fd5849816e
  * https://developer.apple.com/documentation/vision/recognizing_objects_in_live_capture
