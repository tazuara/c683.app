//
//  SearchGeometryCentered.swift
//  C683
//
//  Created by Tristian Azuara on 7/2/23.
//

import Foundation
import CoreGraphics

func findCentered(_ paths: [CGPath], targetPoint: CGPoint) -> (Int, CGPath)? {
    let minIndex = paths.enumerated().map {
        ($0, $1.findCenter())
    }.map {
        ($0, distance($1, targetPoint))
    }.min {
        $1.1 < $0.1
    }?.0
    guard let minIndex = minIndex else { return nil }
    return (minIndex, paths[minIndex])
}

func findClosest(_ paths: [CGPath], targetPoint: CGPoint, minimumArea: CGFloat = .zero) -> (Int, CGPath)? {
    let minIndex = paths.enumerated().map {
        ($0, $1.distances(to: targetPoint), $1.area())
    }.filter {
        $2 > minimumArea
    }.min {
        $0.1.average() < $1.1.average()
    }?.0
    guard let minIndex = minIndex else { return nil }
    return (minIndex, paths[minIndex])
}

/// Calculate the distance between 2 points.
/// - Parameters:
///   - from: The starting point
///   - to: The endoing point
/// - Returns: The distance between them
func distance(_ from: CGPoint, _ to: CGPoint) -> CGFloat {
    (from.x - to.x) * (from.x - to.x) + (from.y - to.y) * (from.y - to.y)
}

fileprivate extension Array where Element == CGFloat {
    func average() -> CGFloat {
        reduce(0, +) / CGFloat(count)
    }
}

extension CGPath {
    func area() -> CGFloat {
        guard !isEmpty else { return 0 }
        let points = getPathElementsPoints()
        guard points.count >= 2 else { return 0 }

        var products = [CGFloat]()
        for index in 1..<points.count {
            products.append((points[index-1].x * points[index].y) - (points[index].x * points[index-1].y))
        }

        return abs(products.reduce(0, +)) / 2.0
    }

    // Based on: https://stackoverflow.com/a/53319889

    func distances(to point: CGPoint) -> [CGFloat] {
        getPathElementsPoints().map {
            distance($0, point)
        }
    }

    private func forEach(body: @escaping @convention(block) (CGPathElement) -> Void) {
        typealias Body = @convention(block) (CGPathElement) -> Void
        let callback: @convention(c) (UnsafeMutableRawPointer, UnsafePointer<CGPathElement>) -> Void = { info, element in
            let body = unsafeBitCast(info, to: Body.self)
            body(element.pointee)
        }
        //print(MemoryLayout.size(ofValue: body))
        let unsafeBody = unsafeBitCast(body, to: UnsafeMutableRawPointer.self)
        apply(info: unsafeBody, function: unsafeBitCast(callback, to: CGPathApplierFunction.self))
    }

    func getPathElementsPoints() -> [CGPoint] {
        var arrayPoints = [CGPoint]()
        forEach { element in
            switch (element.type) {
            case CGPathElementType.moveToPoint:
                arrayPoints.append(element.points[0])
            case .addLineToPoint:
                arrayPoints.append(element.points[0])
            case .addQuadCurveToPoint:
                arrayPoints.append(element.points[0])
                arrayPoints.append(element.points[1])
            case .addCurveToPoint:
                arrayPoints.append(element.points[0])
                arrayPoints.append(element.points[1])
                arrayPoints.append(element.points[2])
            default:
                break
            }
        }
        return arrayPoints
    }

    func getPathElementsPointsAndTypes() -> ([CGPoint],[CGPathElementType]) {
        var arrayPoints = [CGPoint]()
        var arrayTypes = [CGPathElementType]()
        forEach { element in
            switch (element.type) {
            case CGPathElementType.moveToPoint:
                arrayPoints.append(element.points[0])
                arrayTypes.append(element.type)
            case .addLineToPoint:
                arrayPoints.append(element.points[0])
                arrayTypes.append(element.type)
            case .addQuadCurveToPoint:
                arrayPoints.append(element.points[0])
                arrayPoints.append(element.points[1])
                arrayTypes.append(element.type)
                arrayTypes.append(element.type)
            case .addCurveToPoint:
                arrayPoints.append(element.points[0])
                arrayPoints.append(element.points[1])
                arrayPoints.append(element.points[2])
                arrayTypes.append(element.type)
                arrayTypes.append(element.type)
                arrayTypes.append(element.type)
            default: break
            }
        }
        return (arrayPoints,arrayTypes)
    }

    func findCenter() -> CGPoint {
        class Context {
            var sumX: CGFloat = 0
            var sumY: CGFloat = 0
            var points = 0
        }

        var context = Context()

        apply(info: &context) { (context, element) in
            guard let context = context?.assumingMemoryBound(to: Context.self).pointee else {
                return
            }
            switch element.pointee.type {
            case .moveToPoint, .addLineToPoint:
                let point = element.pointee.points[0]
                context.sumX += point.x
                context.sumY += point.y
                context.points += 1
            case .addQuadCurveToPoint:
                let controlPoint = element.pointee.points[0]
                let point = element.pointee.points[1]
                context.sumX += point.x + controlPoint.x
                context.sumY += point.y + controlPoint.y
                context.points += 2
            case .addCurveToPoint:
                let controlPoint1 = element.pointee.points[0]
                let controlPoint2 = element.pointee.points[1]
                let point = element.pointee.points[2]
                context.sumX += point.x + controlPoint1.x + controlPoint2.x
                context.sumY += point.y + controlPoint1.y + controlPoint2.y
                context.points += 3
            case .closeSubpath:
                break
            @unknown default:
                break
            }
        }
        let x = context.sumX / CGFloat(context.points)
        let y = context.sumY / CGFloat(context.points)
        return CGPoint(x: x, y: y)
    }
}

extension CGSize {
    func area() -> CGFloat { width * height }
}

func triangleArea(v1: CGPoint, v2: CGPoint, v3: CGPoint) -> CGFloat {
    let a = distance(v1, v2)
    let b = distance(v2, v3)
    let c = distance(v3, v1)
    return 0.25 * sqrt((a + b + c) * (-a + b + c) * (a - b + c) * (a + b - c))
}

func denormalizePath(_ path: CGPath, sourceFrame: CGSize, targetFrame: CGSize) -> CGPath? {
    var transform = CGAffineTransform.identity
    transform = transform.scaledBy(x: targetFrame.width, y: targetFrame.height)
    let denormalizedPath = path.copy(using: &transform)
    return denormalizedPath
}

func resizePath(_ path: CGPath, sourceFrame: CGSize, targetFrame: CGSize) -> CGPath? {
    let boundingBox = path.boundingBox
    let boundingBoxAspectRatio = boundingBox.width / boundingBox.height
    let viewAspectRatio = targetFrame.width / targetFrame.height
    var scaleFactor = CGFloat(1)

    if boundingBoxAspectRatio > viewAspectRatio {
        scaleFactor = targetFrame.width / boundingBox.width
    } else {
        scaleFactor = targetFrame.height / boundingBox.height
    }

    var scaleTransform = CGAffineTransform.identity
    scaleTransform = scaleTransform.scaledBy(x: scaleFactor, y: scaleFactor)
    scaleTransform.translatedBy(x: -boundingBox.minX, y: -boundingBox.minY)

    let scaledSize = boundingBox.size.applying(CGAffineTransform (scaleX: scaleFactor, y: scaleFactor))
    let centerOffset = CGSize(width: (targetFrame.width - scaledSize.width ) / scaleFactor * 2.0, height: (targetFrame.height - scaledSize.height) / scaleFactor * 2.0)
    scaleTransform = scaleTransform.translatedBy(x: centerOffset.width, y: centerOffset.height)
    //CGPathCreateCopyByTransformingPath(path, &scaleTransform)
    let scaledPath = path.copy(using: &scaleTransform)

    return scaledPath
}
