//
//  DisplayDevice.swift
//  C683
//
//  Created by Tristian Azuara on 7/2/23.
//

import Foundation
import CoreGraphics
import AppKit

struct DisplayDevice {
    static var current: Self { getCurrentDisplay() }

    let size: (width: Measurement<UnitLength>, height: Measurement<UnitLength>)
    let sizeResolution: (horizontal: CGFloat, vertical: CGFloat)
}

private func getCurrentDisplay() -> DisplayDevice {
    let id = CGMainDisplayID()
    let sizeMillis = CGDisplayScreenSize(id)
    let hResolution = sizeMillis.width / NSScreen.main!.frame.size.width
    let vResolution = sizeMillis.height / NSScreen.main!.frame.size.height
    let display = DisplayDevice(size: (width: .init(value: sizeMillis.width, unit: .millimeters),
                                       height: .init(value: sizeMillis.height, unit: .millimeters)),
                                sizeResolution: (hResolution, vResolution))
    return display
}
