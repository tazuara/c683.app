//
//  AreaCaptureView.swift
//  C683
//
//  Created by Tristian Azuara on 6/24/23.
//

import Combine
import SwiftUI
import Vision
import AVFoundation
import AppKit

let yellowColor = CGColor(red: 255, green: 255, blue: 0, alpha: 1)
let redColor = CGColor(red: 2550, green: 0, blue: 0, alpha: 1)

struct AreaCaptureView: NSViewRepresentable {

    typealias NSViewType = VideoPreviewView

    class VideoPreviewView: NSView {
        override func makeBackingLayer() -> CALayer { AVCaptureVideoPreviewLayer() }

        lazy var videoPreviewLayer: AVCaptureVideoPreviewLayer = {
            let backingLayer = makeBackingLayer()
            contoursOverlay.bounds = backingLayer.bounds
            backingLayer.addSublayer(contoursOverlay)
            return backingLayer as! AVCaptureVideoPreviewLayer
        }()

        lazy var contoursOverlay: CALayer = {
            let layer = CALayer()
            layer.name = "ContoursOverlay"
            return layer
        }()
    }

    let outlineColor: CGColor
    let contours: [VNContoursObservation]
    let session: AVCaptureSession
    let bufferSize: CGSize

    @Binding var outputArea: CGFloat

    func makeNSView(context: Context) -> VideoPreviewView {

        let view = VideoPreviewView()
        view.layer = view.videoPreviewLayer
        view.videoPreviewLayer.session = session
        view.videoPreviewLayer.videoGravity = AVLayerVideoGravity.resize
        view.videoPreviewLayer.connection?.videoOrientation = AVCaptureVideoOrientation.portrait

        return view
    }

    func updateNSView(_ nsView: VideoPreviewView, context: Context) {
        print("Updating view", nsView, session)

        if nsView.videoPreviewLayer.session != session {
            nsView.videoPreviewLayer.session = session
        }
        nsView.contoursOverlay.frame = nsView.frame

        drawVisionRequestResults(nsView, results: contours, sourceFrame: bufferSize)
    }

    func drawVisionRequestResults(_ nsView: VideoPreviewView, results: [VNContoursObservation], sourceFrame: CGSize) {
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        defer { CATransaction.commit() }

        nsView.contoursOverlay.sublayers = nil // remove all the old recognized objects

        guard let centeredContour = findCenteredContour(results),
           let resizedPath = denormalizePath(centeredContour.normalizedPath,
                                             sourceFrame: sourceFrame,
                                             targetFrame: nsView.frame.size) else {
               return
        }

        outputArea = resizedPath.area()

        let pathLayer = createOutlineLayer(resizedPath, frame: nsView.frame)
        nsView.contoursOverlay.addSublayer(pathLayer)
    }

    private func findCenteredContour(_ results: [VNContoursObservation]) -> VNContour? {
        for observation in results {
            // Select only the label with the highest confidence.
            let centeredContour = findClosest(observation.topLevelContours.map {
                $0.normalizedPath
            }, targetPoint: .init(x: 0.5, y: 0.5), minimumArea: 0.001)

            guard let centeredContour = centeredContour else { continue }
            return observation.topLevelContours[centeredContour.0]
        }
        return nil
    }

    private func createOutlineLayer(_ path: CGPath, frame: CGRect) -> CALayer {
        let pathLayer = CAShapeLayer()
        pathLayer.strokeColor = outlineColor
        pathLayer.fillColor = .clear
        pathLayer.frame = frame
        pathLayer.path = path
        return pathLayer
    }
}

//struct AreaCaptureView_Previews: PreviewProvider {
//    static var previews: some View {
//        AreaCaptureView(outlineColor: .black,
//                        contours: [],
//                        session: .init(),
//                        bufferSize: .init(width: 1024, height: 1024),
//                        outputArea: .constant(.init(value: 15, unit: .squareCentimeters)),
//                        outputUnitArea: .constant(.init(symbol: "x", converter: UnitConverterLinear(coefficient: 1))))
//            .frame(height: 300)
//    }
//}
