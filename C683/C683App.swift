//
//  C683App.swift
//  C683
//
//  Created by Tristian Azuara on 6/24/23.
//

import SwiftUI

@main
struct C683App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
