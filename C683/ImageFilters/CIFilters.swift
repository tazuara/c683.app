//
//  CIFilters.swift
//  C683
//
//  Created by Tristian Azuara on 7/2/23.
//

import Foundation
import CoreImage

class CIFilterMetalBlackAndWhite: CIFilter {
    @objc dynamic var inputImage: CIImage?

    override public var outputImage: CIImage! {
        guard let inputImage = self.inputImage else { return nil }
        let args = [inputImage as AnyObject]
        let callback: CIKernelROICallback = { (_, rect) in rect }
        return Self.kernel.apply(extent: inputImage.extent, roiCallback: callback, arguments: args)
    }

    static let kernel: CIColorKernel = { () -> CIColorKernel in
        let url = Bundle.main.url(forResource: "default", withExtension: "metallib")! //1
        let data = try! Data(contentsOf: url)
        return try! CIColorKernel(functionName: "grayscaleFilterKernel",
                                  fromMetalLibraryData: data) //2
    }()
}

class SequencedFilters: CIFilter {
    @objc dynamic var inputImage: CIImage?
    var filters: [CIFilter]

    init(filters: [CIFilter]) {
        self.filters = filters
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override public var outputImage: CIImage! {
        guard let inputImage = self.inputImage, !filters.isEmpty else { return inputImage }
        filters.first!.setValue(inputImage, forKey: kCIInputImageKey)
        var resultImage = filters.first!.outputImage
        for index in 1..<filters.count {
            filters[index].setValue(resultImage, forKey: kCIInputImageKey)
            resultImage = filters[index].outputImage
        }
        return resultImage
    }
}
