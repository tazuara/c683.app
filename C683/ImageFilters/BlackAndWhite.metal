//
//  BlackAndWhite.metal
//  C683
//
//  Created by Tristian Azuara on 7/1/23.
//

#include <metal_stdlib>
#include <CoreImage/CoreImage.h>
using namespace metal;

extern "C" {
  namespace coreimage {
      float4 grayscaleFilterKernel(sample_t s) { //1
        float gray = (s.r + s.g + s.b) / 3;      //2
        return float4(gray, gray, gray, s.a);    //3
      }
  }
}
