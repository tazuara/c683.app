//
//  CameraService.swift
//  C683
//
//  Created by Tristian Azuara on 6/24/23.
//
import AVFoundation
import Foundation
import Vision
import AppKit
import CoreImage.CIFilterBuiltins

typealias ImageBufferHandler = ((_ imageBuffer: CMSampleBuffer) -> ())

class CameraService: NSObject, ObservableObject, AVCaptureVideoDataOutputSampleBufferDelegate {
    let videoDataOutput = AVCaptureVideoDataOutput()

    @Published var contourResults: [VNContoursObservation] = []
    @Published var bufferSize = CGSize.zero

    private(set) var session = AVCaptureSession()
    private var videoConnection: AVCaptureConnection!

    var devices: [AVCaptureDevice] {
        AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera, .externalUnknown],
                                         mediaType: .video,
                                         position: .unspecified).devices
    }

    let videoProcessingQueue = DispatchQueue(label: "com.wgu.c683.queue.VideoProcessing")
    private lazy var contourDetectionRequestQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.underlyingQueue = videoProcessingQueue
        queue.qualityOfService = .userInteractive
        return queue
    }()

    private lazy var contourRecognitionRequest: VNDetectContoursRequest = {
        let contourRequest = VNDetectContoursRequest(completionHandler: self.handleContourRequest)
        contourRequest.revision = VNDetectContourRequestRevision1

        contourRequest.contrastAdjustment = 3.0
        contourRequest.detectsDarkOnLight = false
        contourRequest.maximumImageDimension = Int.max
        return contourRequest
    }()

    func configure() {
        // setup video output
        let videoDataOutput = AVCaptureVideoDataOutput()
        videoDataOutput.videoSettings = [
            kCVPixelBufferPixelFormatTypeKey: NSNumber(value: kCVPixelFormatType_32BGRA)
        ] as [String : Any]

        videoDataOutput.alwaysDiscardsLateVideoFrames = true
        videoDataOutput.setSampleBufferDelegate(self, queue: videoProcessingQueue)
        guard session.canAddOutput(videoDataOutput) else {
            fatalError()
        }
        session.addOutput(videoDataOutput)

        videoConnection = videoDataOutput.connection(with: .video)
    }

    func configure(videoSource where: @escaping (AVCaptureDevice) -> Bool) {
        guard let webcamDevice = devices.first(where: `where`) else {
            return removeVideoSource()
        }

        print(webcamDevice.localizedName)

        if session.isRunning {
            session.stopRunning()
        }

        session.beginConfiguration()
        let webcamInput = try! AVCaptureDeviceInput(device: webcamDevice)
        if session.canAddInput(webcamInput) {
            session.addInput(webcamInput)
        }

        if session.canAddOutput(videoDataOutput) {
            session.addOutput(videoDataOutput)
            // Add a video data output
            videoDataOutput.alwaysDiscardsLateVideoFrames = true
            videoDataOutput.videoSettings = [
                kCVPixelBufferPixelFormatTypeKey as String: Int(kCVPixelFormatType_420YpCbCr8BiPlanarFullRange)
            ]
            videoDataOutput.setSampleBufferDelegate(self, queue: videoProcessingQueue)
        } else {
            print("Could not add video data output to the session")
            session.commitConfiguration()
            return
        }

        let captureConnection = videoDataOutput.connection(with: .video)
        // Always process the frames
        captureConnection?.isEnabled = true
        do {
            try webcamDevice.lockForConfiguration()
            let dimensions = CMVideoFormatDescriptionGetDimensions(webcamDevice.activeFormat.formatDescription)
            bufferSize = .init(width: CGFloat(dimensions.width), height: CGFloat(dimensions.height))
            webcamDevice.unlockForConfiguration()
        } catch {
            print(error)
        }

        session.commitConfiguration()
        session.startRunning()
    }

    func configure(previewSize: CGSize) {
        
    }

    private func removeVideoSource() {
        session.stopRunning()
        session.beginConfiguration()
        session.inputs.forEach { session.removeInput($0) }
        session.commitConfiguration()
    }

    private var threshold = Threshold(startingValue: 200, limit: 200) // Start at limit to process first frame

    let contourPreprocessingFilters = SequencedFilters(filters: [
        CIFilterMetalBlackAndWhite(),
        {
            let nrFilter = CIFilter.noiseReduction()
            nrFilter.noiseLevel = 0
            nrFilter.sharpness = 1
            return nrFilter
        }(),

    ])

    func captureOutput(_ output: AVCaptureOutput,
                       didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard threshold.increment() else { return }
        print("Capture!")

        guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            return
        }

        let ciImage = CIImage(cvImageBuffer: pixelBuffer)
        contourPreprocessingFilters.inputImage = ciImage

        guard let bwCIImage = contourPreprocessingFilters.outputImage else { return }
        guard let bwPixelBuffer = buffer(from: bwCIImage,
                                         width: Int(bufferSize.width),
                                         height: Int(bufferSize.height)) else { return }

        contourDetectionRequestQueue.addOperation {
            let requestHandler = VNImageRequestHandler(cvPixelBuffer: bwPixelBuffer, orientation: .up, options: [:])
            do {
                try requestHandler.perform([self.contourRecognitionRequest])
            } catch {
                print(error)
            }
        }
    }

    private func handleContourRequest(_ request: VNRequest, _ error: Error?) {
        if let error = error {
            contourResults = []
            return print(#function, error)
        }
        DispatchQueue.main.async {
            self.contourResults = request.results?.map { $0 as? VNContoursObservation }.compactMap { $0 } ?? []
            print(self.contourResults)
        }
    }

    let context = CIContext()

    // https://stackoverflow.com/q/54354138
    // https://stackoverflow.com/q/44462087
    func buffer(from image: CIImage, width: Int, height: Int) -> CVPixelBuffer? {
        var pixelbuffer: CVPixelBuffer?

        CVPixelBufferCreate(kCFAllocatorDefault, width, height, kCVPixelFormatType_OneComponent8, nil, &pixelbuffer)
        CVPixelBufferLockBaseAddress(pixelbuffer!, CVPixelBufferLockFlags(rawValue: 0))

        let colorspace = CGColorSpaceCreateDeviceGray()
        let bitmapContext = CGContext(data: CVPixelBufferGetBaseAddress(pixelbuffer!),
                                      width: width,
                                      height: height,
                                      bitsPerComponent: 8,
                                      bytesPerRow: CVPixelBufferGetBytesPerRow(pixelbuffer!),
                                      space: colorspace,
                                      bitmapInfo: 0)!
        guard let cgImage = context.createCGImage(image, from: image.extent) else { return nil }
        bitmapContext.draw(cgImage, in: CGRect(x: 0, y: 0, width: width, height: height))

        return pixelbuffer
    }
}

fileprivate struct Threshold {
    private var value: Int
    let limit: Int

    init(startingValue: Int, limit: Int) {
        self.value = startingValue
        self.limit = limit
    }

    mutating func increment(by delta: Int = 1) -> Bool {
        value += delta
        if value > limit {
            value = 0
            return true
        } else {
            return false
        }
    }
}

extension AVCaptureDevice: Identifiable {}

