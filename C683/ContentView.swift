//
//  ContentView.swift
//  C683
//
//  Created by Tristian Azuara on 6/24/23.
//

import SwiftUI
import AVFoundation

struct ContentView: View {
    @ObservedObject var cameraService = CameraService()

    @State var videoSourceIndex = -1
    @State var cameraAuthorizationStatus: AVAuthorizationStatus? = nil
    @State var outlineColor = OutlineColorOption.red

    @State var referenceSurfaceArea = "25"
    @State var referenceSurfaceAreaLocked: Bool = false
    @State var referenceBaseLength: CGFloat = 1
    @State var viewportBaseLength: CGFloat = 1

    @State var viewportArea = CGFloat.zero
    @State var viewportSize: CGSize = .zero

    var body: some View {
        NavigationView {
            VStack(alignment: .leading) {
                Text("Readings").font(.title2)
                Picker("Video Source", selection: $videoSourceIndex) {
                    Text("----").tag(-1)
                    ForEach(cameraService.devices, id: \.id) {
                        Text($0.localizedName).tag($0.id.hashValue)
                    }
                }.onChange(of: videoSourceIndex) { selection in
                    cameraService.configure(videoSource: {
                        $0.id.hashValue == selection
                    })
                }
                Picker("Outline Color", selection: $outlineColor) {
                    ForEach(OutlineColorOption.allCases) {
                        Text($0.description).tag($0.id)
                    }
                }
                referenceViews
                Divider()
                readingViews
                Spacer()
            }.padding(24)
            GeometryObserver { geometry in
                viewportSize = geometry.size
            } content: { _ in
                AreaCaptureView(outlineColor: outlineColor.cgColor,
                                contours: cameraService.contourResults,
                                session: cameraService.session,
                                bufferSize: cameraService.bufferSize,
                                outputArea: $viewportArea)
                    .background(Color.black)
            }
        }.frame(minWidth: 450, alignment: .leading)
    }

    @ViewBuilder
    private var readingViews: some View {
        HStack {
            Text("Viewport Area")
            Text(viewportArea > 0 ? "\(viewportArea)" : "--")
        }.padding(.top, 16)
        HStack {
            Text("Physical Area")
            Text(physicalArea > 0 ? "\(physicalArea)" : "--")
        }.padding(.top, 16)
    }

    @ViewBuilder
    private var referenceViews: some View {
        HStack {
            Text("Reference Area")
            TextField(referenceSurfaceArea, value: $referenceSurfaceArea, formatter: NumberFormatter())
                .frame(width: 50)
                .disabled(referenceSurfaceAreaLocked)
            Toggle("Lock", isOn: $referenceSurfaceAreaLocked)
                .toggleStyle(.switch)
                .onChange(of: referenceSurfaceAreaLocked) { newValue in
                    if newValue, let squaredRef = Double(referenceSurfaceArea) {
                        referenceBaseLength = sqrt(squaredRef)
                        viewportBaseLength = sqrt(viewportArea)
                    }
                }
        }.padding(.top, 16)
        HStack {
            Text("Physical Length")
            Text("\(referenceBaseLength)")
        }.padding(.top, 16)
        HStack {
            Text("Viewport Length")
            Text("\(viewportBaseLength)")
        }.padding(.top, 16)
        HStack {
            Text("Viewport Size")
            Text("\(Int(viewportSize.width))x\(Int(viewportSize.height))")
        }.padding(.top, 16)
    }

    private var physicalArea: CGFloat {
        pow(sqrt(viewportArea) / viewportBaseLength * referenceBaseLength, 2)
    }

    private var aspectRatio: CGFloat {
        CGFloat(cameraService.bufferSize.width) / CGFloat(cameraService.bufferSize.height)
    }
}

struct GeometryObserver<Content>: View where Content: View {
    let action: ((GeometryProxy) -> Void)?
    let content: (GeometryProxy) -> Content

    init(action: ((GeometryProxy) -> Void)? = nil, content: @escaping (GeometryProxy) -> Content) {
        self.action = action
        self.content = content
    }

    var body: some View {
        GeometryReader { geometry in
            self.resolve(geometry)
        }
    }

    private func resolve(_ geometry: GeometryProxy) -> some View {
        action?(geometry)
        return content(geometry)
    }
}

enum OutlineColorOption: Int, Identifiable, CaseIterable {
    typealias ObjectIdentifier = Self
    case red, yellow, blue, black, white

    var id: ObjectIdentifier { self }

    var cgColor: CGColor {
        switch self {
        case .red: return CGColor(red: 255, green: 0, blue: 0, alpha: 1)
        case .yellow: return CGColor(red: 255, green: 255, blue: 0, alpha: 1)
        case .blue: return CGColor(red: 0, green: 0, blue: 255, alpha: 1)
        case .black: return CGColor.black
        case .white: return CGColor.white
        }
    }

    var description: String {
        switch self {
        case .red: return "Red"
        case .yellow: return "Yellow"
        case .blue: return "Blue"
        case .black: return "Black"
        case .white: return "White"
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
